use rand::Rng;

use crate::token_generator::TokenGenerator;

pub struct RealTokenGenerator {}

impl RealTokenGenerator {
    pub fn new() -> RealTokenGenerator {
        RealTokenGenerator {}
    }

    fn gen(&self, len: usize) -> String {
        let mut rng = rand::thread_rng();
        let mut bytes: Vec<u8> = Vec::with_capacity(len);
        for _ in 0..len {
            bytes.push(rng.gen())
        }
        base64::encode_config(bytes, base64::URL_SAFE_NO_PAD)
    }
}

impl TokenGenerator for RealTokenGenerator {
    fn generate_login_token(&self) -> String {
        self.gen(48)
    }

    fn generate_registration_code(&self) -> String {
        self.gen(4)
    }
}

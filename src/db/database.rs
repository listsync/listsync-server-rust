use std::time::SystemTime;

use crate::data::{
    Error, ListInfo, ListItem, NewItem, NewUser, ReplacementItem, UpdateUser,
    UserResponse, UserTokenResponse,
};

pub trait Database: Send + Sync {
    fn is_correct_password(&self, username: &str, password: &str) -> bool;

    fn is_correct_token(
        &self,
        username: &str,
        token: &str,
        now: SystemTime,
    ) -> bool;

    fn add_user(
        &self,
        new_user: &NewUser,
        registration_code: Option<String>,
    ) -> Result<UserResponse, Error>;

    fn update_user(
        &self,
        username: &str,
        update_user: &UpdateUser,
    ) -> Result<UserResponse, Error>;

    fn add_user_token(
        &self,
        username: &str,
        token: &str,
        expiry_time: SystemTime,
    ) -> Result<UserTokenResponse, Error>;

    fn delete_user_token(
        &self,
        username: &str,
        token: &str,
    ) -> Result<(), Error>;

    fn set_user_registered(&self, username: &str) -> Result<(), Error>;
    fn delete_user(&self, username: &str) -> Result<(), Error>;
    fn count_users(&self) -> usize;
    fn get_user(&self, username: &str) -> Result<UserResponse, Error>;
    fn get_lists(&self, username: &str) -> Result<Vec<ListInfo>, Error>;

    fn add_list(&self, username: &str, title: &str) -> Result<ListInfo, Error>;

    fn update_list(
        &self,
        username: &str,
        listid: &str,
        title: &str,
    ) -> Result<ListInfo, Error>;

    fn delete_list(&self, username: &str, listid: &str) -> Result<(), Error>;

    fn get_list(&self, username: &str, listid: &str)
        -> Result<ListInfo, Error>;

    fn set_items(
        &self,
        username: &str,
        listid: &str,
        new_items: &Vec<NewItem>,
    ) -> Result<Vec<ListItem>, Error>;

    fn get_items(
        &self,
        username: &str,
        listid: &str,
    ) -> Result<Vec<ListItem>, Error>;

    fn add_item(
        &self,
        username: &str,
        listid: &str,
        new_item: &NewItem,
    ) -> Result<ListItem, Error>;

    fn delete_item(
        &self,
        username: &str,
        listid: &str,
        itemid: &str,
    ) -> Result<(), Error>;

    fn get_item(
        &self,
        username: &str,
        listid: &str,
        itemid: &str,
    ) -> Result<ListItem, Error>;

    fn set_item(
        &self,
        username: &str,
        listid: &str,
        itemid: &str,
        replacement_item: &ReplacementItem,
    ) -> Result<ListItem, Error>;

    fn get_cell(
        &self,
        username: &str,
        listid: &str,
        itemid: &str,
        columnname: &str,
    ) -> Result<serde_json::Value, Error>;

    fn set_cell(
        &self,
        username: &str,
        listid: &str,
        itemid: &str,
        columnname: &str,
        value: &serde_json::Value,
    ) -> Result<(), Error>;
}

use serde::{Deserialize, Serialize};
use std::collections::HashMap;

use crate::data::Error;
use crate::db::memory_database::{User, UserInfo};

#[derive(Default, Deserialize, Serialize)]
pub struct Users {
    #[serde(flatten)]
    username2user: HashMap<String, User>,
}

impl Users {
    pub fn new() -> Users {
        Users {
            username2user: HashMap::new(),
        }
    }

    pub fn add(
        &mut self,
        username: &str,
        hashed_password: String,
        admin: bool,
        registration_code: Option<String>,
    ) -> Result<UserInfo, Error> {
        if self.username2user.contains_key(username) {
            Err(Error::UserAlreadyExists)
        } else {
            let info = UserInfo::new(
                username,
                hashed_password,
                admin,
                registration_code,
            );
            self.username2user
                .insert(String::from(username), User::new(info.clone()));
            Ok(info)
        }
    }

    pub fn update(
        &mut self,
        username: &str,
        hashed_password: Option<String>,
        admin: Option<bool>,
    ) -> Result<UserInfo, Error> {
        self.username2user
            .get_mut(username)
            .map(|user| {
                if let Some(hp) = hashed_password {
                    user.info.hashed_password = hp;
                }
                if let Some(adm) = admin {
                    user.info.admin = adm
                }
                user.info.clone()
            })
            .ok_or(Error::UserDoesNotExist)
    }

    pub fn delete(&mut self, username: &str) -> Result<(), Error> {
        self.username2user
            .remove(username)
            .ok_or(Error::UserDoesNotExist)
            .map(|_| ())
    }

    pub fn get(&self, username: &str) -> Result<&User, Error> {
        self.username2user
            .get(username)
            .ok_or(Error::UserDoesNotExist)
    }

    pub fn get_mut(&mut self, username: &str) -> Result<&mut User, Error> {
        self.username2user
            .get_mut(username)
            .ok_or(Error::UserDoesNotExist)
    }

    pub fn len(&self) -> usize {
        self.username2user.len()
    }
}

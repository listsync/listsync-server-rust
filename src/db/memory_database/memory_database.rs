use std::time::SystemTime;

use crate::data::{
    Error, ListInfo, ListItem, NewItem, NewUser, ReplacementItem, UpdateUser,
    UserResponse, UserTokenResponse,
};
use crate::db::memory_database::Users;
use crate::db::Database;
use crate::idgen::IdGen;
use crate::password_handler::PasswordHandler;
use crate::save::{SaveOnChange, Saver};

pub struct MemoryDatabase {
    users: SaveOnChange<Users>,
    password_handler: &'static dyn PasswordHandler,
    idgen: Box<dyn IdGen>,
}

impl MemoryDatabase {
    pub fn new(
        saver: Box<dyn Saver<Users>>,
        password_handler: &'static dyn PasswordHandler,
        idgen: Box<dyn IdGen>,
    ) -> MemoryDatabase {
        MemoryDatabase {
            users: SaveOnChange::new(saver.load(), saver),
            password_handler,
            idgen,
        }
    }
}

impl Database for MemoryDatabase {
    fn is_correct_password(&self, username: &str, password: &str) -> bool {
        self.users
            .read_lock()
            .get(username)
            .map(|user| {
                self.password_handler
                    .is_correct_password(password, &user.info.hashed_password)
            })
            .unwrap_or(false)
    }

    fn is_correct_token(
        &self,
        username: &str,
        token: &str,
        now: SystemTime,
    ) -> bool {
        // We use nonsaving_write_lock here because we may modify the DB if
        // tokens have expired, but we don't care if that change is lost
        // and we have to re-do it next time.
        self.users
            .nonsaving_write_lock()
            .get_mut(username)
            .map(|user| user.tokens.is_valid(token, now))
            .unwrap_or(false)
    }

    fn add_user(
        &self,
        new_user: &NewUser,
        registration_code: Option<String>,
    ) -> Result<UserResponse, Error> {
        self.users
            .write_lock()
            .add(
                &new_user.username,
                self.password_handler.hash_password(&new_user.password),
                new_user.admin,
                registration_code,
            )
            .map(|user_info| user_info.to_response())
    }

    fn update_user(
        &self,
        username: &str,
        update_user: &UpdateUser,
    ) -> Result<UserResponse, Error> {
        self.users
            .write_lock()
            .update(
                username,
                match &update_user.password {
                    None => None,
                    Some(pw) => Some(self.password_handler.hash_password(&pw)),
                },
                update_user.admin,
            )
            .map(|user_info| user_info.to_response())
    }

    fn add_user_token(
        &self,
        username: &str,
        token: &str,
        expiry_time: SystemTime,
    ) -> Result<UserTokenResponse, Error> {
        self.users
            .write_lock()
            .get_mut(username)
            .map(|user| user.tokens.add(username, token, expiry_time))
    }

    fn delete_user_token(
        &self,
        username: &str,
        token: &str,
    ) -> Result<(), Error> {
        self.users
            .write_lock()
            .get_mut(username)
            .and_then(|user| user.tokens.delete(token))
    }

    fn set_user_registered(&self, username: &str) -> Result<(), Error> {
        self.users
            .write_lock()
            .get_mut(username)
            .map(|user| user.info.registration_code = None)
    }

    fn delete_user(&self, username: &str) -> Result<(), Error> {
        self.users.write_lock().delete(&username)
    }

    fn count_users(&self) -> usize {
        self.users.read_lock().len()
    }

    fn get_user(&self, username: &str) -> Result<UserResponse, Error> {
        self.users
            .read_lock()
            .get(username)
            .map(|user| user.info.to_response())
    }

    fn get_lists(&self, username: &str) -> Result<Vec<ListInfo>, Error> {
        self.users
            .read_lock()
            .get(username)
            .map(|user| user.lists.all())
    }

    fn add_list(&self, username: &str, title: &str) -> Result<ListInfo, Error> {
        self.users.write_lock().get_mut(username).map(|user| {
            user.lists.add(user.generate_listid(&*self.idgen), title)
        })
    }

    fn update_list(
        &self,
        username: &str,
        listid: &str,
        title: &str,
    ) -> Result<ListInfo, Error> {
        self.users.write_lock().get_mut(username).and_then(|user| {
            user.lists.get_mut(listid).map(|list| {
                list.info.title = String::from(title);
                list.info.clone()
            })
        })
    }

    fn delete_list(&self, username: &str, listid: &str) -> Result<(), Error> {
        self.users
            .write_lock()
            .get_mut(username)
            .and_then(|user| user.lists.delete(listid))
    }

    fn get_list(
        &self,
        username: &str,
        listid: &str,
    ) -> Result<ListInfo, Error> {
        self.users
            .read_lock()
            .get(username)
            .and_then(|user| user.lists.get(listid))
            .map(|list| list.info.clone())
    }

    fn set_items(
        &self,
        username: &str,
        listid: &str,
        new_items: &Vec<NewItem>,
    ) -> Result<Vec<ListItem>, Error> {
        self.users
            .write_lock()
            .get_mut(username)
            .and_then(|user| user.lists.get_mut(listid))
            .map(|list| list.replace_all(new_items).clone())
    }

    fn get_items(
        &self,
        username: &str,
        listid: &str,
    ) -> Result<Vec<ListItem>, Error> {
        self.users
            .read_lock()
            .get(username)
            .and_then(|user| user.lists.get(listid))
            .map(|list| list.items.clone())
    }

    fn add_item(
        &self,
        username: &str,
        listid: &str,
        new_item: &NewItem,
    ) -> Result<ListItem, Error> {
        self.users
            .write_lock()
            .get_mut(username)
            .and_then(|user| user.lists.get_mut(listid))
            .map(|list| list.add(new_item, &vec![]))
    }

    fn delete_item(
        &self,
        username: &str,
        listid: &str,
        itemid: &str,
    ) -> Result<(), Error> {
        self.users
            .write_lock()
            .get_mut(username)
            .and_then(|user| user.lists.get_mut(listid))
            .and_then(|list| list.delete(itemid))
    }

    fn get_item(
        &self,
        username: &str,
        listid: &str,
        itemid: &str,
    ) -> Result<ListItem, Error> {
        self.users
            .read_lock()
            .get(username)
            .and_then(|user| user.lists.get(listid))
            .and_then(|list| list.find(itemid))
    }

    fn set_item(
        &self,
        username: &str,
        listid: &str,
        itemid: &str,
        replacement_item: &ReplacementItem,
    ) -> Result<ListItem, Error> {
        self.users
            .write_lock()
            .get_mut(username)
            .and_then(|user| user.lists.get_mut(listid))
            .and_then(|list| {
                list.find(itemid).and_then(|_| {
                    list.replace(
                        itemid,
                        replacement_item.order,
                        replacement_item.ticked,
                        &replacement_item.text,
                    )
                })
            })
    }

    fn get_cell(
        &self,
        username: &str,
        listid: &str,
        itemid: &str,
        columnname: &str,
    ) -> Result<serde_json::Value, Error> {
        self.users
            .read_lock()
            .get(username)
            .and_then(|user| user.lists.get(listid))
            .and_then(|list| list.find(itemid))
            .and_then(|item| item.get(columnname))
    }

    fn set_cell(
        &self,
        username: &str,
        listid: &str,
        itemid: &str,
        columnname: &str,
        value: &serde_json::Value,
    ) -> Result<(), Error> {
        self.users
            .write_lock()
            .get_mut(username)
            .and_then(|user| user.lists.get_mut(listid))
            .and_then(|list| {
                let new_item: Result<ListItem, Error> = list
                    .find(itemid)
                    .and_then(|mut item_info| item_info.set(columnname, value));
                new_item.and_then(|item| {
                    list.replace(itemid, item.order, item.ticked, &item.text)
                })
            })
            .map(|_| ())
    }
}

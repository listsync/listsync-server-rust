use serde::{Deserialize, Serialize};
use sha2::{Digest, Sha256};
use std::collections::HashMap;
use std::time::SystemTime;

use crate::data::{Error, UserTokenResponse};

#[derive(Deserialize, Serialize)]
pub struct Tokens {
    #[serde(flatten)]
    tokentoexpiry: HashMap<String, SystemTime>,
}

impl Tokens {
    pub fn new() -> Tokens {
        Tokens {
            tokentoexpiry: HashMap::new(),
        }
    }

    pub fn add(
        &mut self,
        username: &str,
        token: &str,
        expiry: SystemTime,
    ) -> UserTokenResponse {
        self.tokentoexpiry.insert(hash(token), expiry);

        UserTokenResponse {
            username: String::from(username),
            token: String::from(token),
        }
    }

    pub fn delete(&mut self, token: &str) -> Result<(), Error> {
        self.tokentoexpiry
            .remove(&hash(token))
            .ok_or(Error::TokenDoesNotExist)
            .map(|_| ())
    }

    pub fn is_valid(&mut self, token: &str, now: SystemTime) -> bool {
        // Delete expired tokens
        self.tokentoexpiry.retain(|_, expiry| *expiry > now);

        // If this token was there, and is still there after deleting
        // expired ones, it is valid.
        self.tokentoexpiry.contains_key(&hash(token))
    }
}

impl Default for Tokens {
    fn default() -> Self {
        Tokens::new()
    }
}

fn hash(input: &str) -> String {
    base64::encode(Sha256::digest(input.as_bytes()))
}

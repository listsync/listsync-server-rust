use serde::{Deserialize, Serialize};

use crate::data::UserResponse;

#[derive(Clone, Deserialize, Serialize)]
pub struct UserInfo {
    pub username: String,
    pub hashed_password: String,
    pub admin: bool,
    pub registration_code: Option<String>,
}

impl UserInfo {
    pub fn new(
        username: &str,
        hashed_password: String,
        admin: bool,
        registration_code: Option<String>,
    ) -> UserInfo {
        UserInfo {
            username: String::from(username),
            hashed_password,
            admin,
            registration_code,
        }
    }

    pub fn to_response(&self) -> UserResponse {
        UserResponse {
            username: self.username.clone(),
            admin: self.admin,
            registration_code: self.registration_code.clone(),
        }
    }
}

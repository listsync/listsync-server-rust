use serde::{Deserialize, Serialize};
use std::collections::HashMap;

use crate::data::{Error, ListInfo};
use crate::db::memory_database::List;

#[derive(Deserialize, Serialize)]
pub struct Lists {
    #[serde(flatten)]
    listid2list: HashMap<String, List>,
}

impl Lists {
    pub fn new() -> Lists {
        Lists {
            listid2list: HashMap::new(),
        }
    }

    pub fn all(&self) -> Vec<ListInfo> {
        let mut ret: Vec<ListInfo> = self
            .listid2list
            .values()
            .map(|list| list.info.clone())
            .collect();
        ret.sort_by_key(|info| info.title.clone());
        ret
    }

    pub fn get(&self, listid: &str) -> Result<&List, Error> {
        self.listid2list.get(listid).ok_or(Error::ListDoesNotExist)
    }

    pub fn get_mut(&mut self, listid: &str) -> Result<&mut List, Error> {
        self.listid2list
            .get_mut(listid)
            .ok_or(Error::ListDoesNotExist)
    }

    pub fn add(&mut self, listid: String, title: &str) -> ListInfo {
        let info = ListInfo {
            listid: String::from(&listid),
            title: String::from(title),
        };
        self.listid2list.insert(listid, List::new(info.clone()));
        info
    }

    pub fn delete(&mut self, listid: &str) -> Result<(), Error> {
        self.listid2list
            .remove(listid)
            .ok_or(Error::ListDoesNotExist)
            .map(|_| ())
    }

    pub fn contains(&self, listid: &str) -> bool {
        self.listid2list.contains_key(listid)
    }
}

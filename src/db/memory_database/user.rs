use serde::{Deserialize, Serialize};

use crate::db::memory_database::{Lists, Tokens, UserInfo};
use crate::idgen::IdGen;

#[derive(Deserialize, Serialize)]
pub struct User {
    pub info: UserInfo,
    pub lists: Lists,
    #[serde(default)]
    pub tokens: Tokens,
}

impl User {
    pub fn new(info: UserInfo) -> User {
        User {
            info,
            lists: Lists::new(),
            tokens: Tokens::new(),
        }
    }

    pub fn generate_listid(&self, idgen: &dyn IdGen) -> String {
        loop {
            let ret = idgen.gen();
            if !self.lists.contains(&ret) {
                return ret;
            }
        }
    }
}

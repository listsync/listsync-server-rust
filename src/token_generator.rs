pub trait TokenGenerator: Send + Sync {
    fn generate_login_token(&self) -> String;
    fn generate_registration_code(&self) -> String;
}

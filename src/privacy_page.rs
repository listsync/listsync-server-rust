use std::fs::File;
use std::io::Read;

#[derive(Clone)]
pub struct PrivacyPage {
    pub contents: String,
}

const DEFAULT: &str = r#"The privacy policy for this server
has not been provided.  Please get in touch with the person who set
up this server, or see
<a href="https://gitlab.com/listsync/listsync-server-rust">gitlab.com/listsync/listsync-server-rust</a>
for more information about the software that is running on this server."#;

impl PrivacyPage {
    pub fn new(contents: String) -> PrivacyPage {
        PrivacyPage { contents }
    }

    pub fn load(filename: &str) -> PrivacyPage {
        let file = File::open(filename);

        if let Ok(mut file) = file {
            let mut contents = String::new();
            let res = file.read_to_string(&mut contents);
            if let Ok(_) = res {
                println!("Loaded privacy policy from {}", filename);
                return PrivacyPage::new(contents);
            }
        }
        println!(
            "No privacy policy was provided - create a file called {}",
            filename
        );
        PrivacyPage::new(String::from(DEFAULT))
    }
}

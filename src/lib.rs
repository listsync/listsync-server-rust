mod api;
pub mod authentication;
pub mod authorization;
pub mod clock;
pub mod data;
pub mod db;
pub mod idgen;
mod logged_in_user;
pub mod password_handler;
pub mod privacy_page;
pub mod real_clock;
pub mod real_idgen;
pub mod real_password_handler;
pub mod real_token_generator;
mod response;
pub mod save;
pub mod state;
pub mod token_generator;

use actix_web_httpauth::middleware::HttpAuthentication;

use actix_web::web;

/**
 * Unauthenticated APIs:
 * /v1/setup   - for creating the first user - may
 *               only be used if no users exist.  I.e. this is a
 *               bootstrapping mechanism.
 * /v1/newuser - user registration
 * /v1/privacy - information about the server's privacy policy
 */
pub fn config_unauthenticated(cfg: &mut web::ServiceConfig) {
    cfg.service(web::scope("/v1/setup").configure(api::v1::setup::config))
        .service(web::scope("/v1/newuser").configure(api::v1::newuser::config))
        .service(web::scope("/v1/privacy").configure(api::v1::privacy::config));
}

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::scope("/v1/list")
            .configure(api::v1::list::all_lists::config)
            .configure(api::v1::list::single_list::config)
            .configure(api::v1::list::single_item::config)
            .configure(api::v1::list::single_cell::config)
            .wrap(HttpAuthentication::basic(authentication::authenticate)),
    )
    .service(
        web::scope("/v1/login/{username}")
            .route("", web::delete().to(api::v1::login::delete))
            .wrap(HttpAuthentication::basic(authentication::authenticate)),
    )
    .service(
        web::scope("/v1/login")
            .route("", web::post().to(api::v1::login::post))
            .wrap(HttpAuthentication::basic(
                authentication::authenticate_with_password,
            )),
    )
    .service(
        web::scope("/v1/user")
            .configure(api::v1::user::all_users::config)
            .configure(api::v1::user::single_user::config)
            .wrap(HttpAuthentication::basic(authentication::authenticate)),
    );
}

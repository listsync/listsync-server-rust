use actix_cors::{Cors, CorsFactory};
use actix_web::{middleware, web, App, HttpServer};

use listsync_server_rust;
use listsync_server_rust::db::memory_database::MemoryDatabase;
use listsync_server_rust::privacy_page::PrivacyPage;
use listsync_server_rust::real_clock::RealClock;
use listsync_server_rust::real_idgen::RealIdGen;
use listsync_server_rust::real_password_handler::RealPasswordHandler;
use listsync_server_rust::real_token_generator::RealTokenGenerator;
use listsync_server_rust::save::FileSaver;
use listsync_server_rust::state::State;

const REAL_PASSWORD_HANDLER: RealPasswordHandler = RealPasswordHandler {};

struct Args {
    bind: String,
    port: String,
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::init();

    let privacy_page = PrivacyPage::load("privacy.html");
    let args = cmdline_args();
    let file_saver = FileSaver::new("state.json");
    let mem_db = MemoryDatabase::new(
        Box::new(file_saver.clone()),
        &REAL_PASSWORD_HANDLER,
        Box::new(RealIdGen::new()),
    );
    let token_generator = RealTokenGenerator::new();
    let clock = RealClock::new();
    let data = web::Data::new(State::new(
        Box::new(mem_db),
        Box::new(token_generator),
        Box::new(clock),
        privacy_page,
    ));

    println!("Listening on http://{}:{}", args.bind, args.port);
    let res = HttpServer::new(move || {
        App::new()
            .wrap(cors())
            .wrap(middleware::NormalizePath)
            .app_data(data.clone())
            .wrap(middleware::Logger::default())
            .configure(listsync_server_rust::config_unauthenticated)
            .configure(listsync_server_rust::config)
    })
    .bind(format!("{}:{}", args.bind, args.port))?
    .run()
    .await;

    file_saver.quit();

    res
}

fn cmdline_args() -> Args {
    let matches = clap::App::new("listsync-server-rust")
        .version(&clap::crate_version!()[..])
        .about(
            "A server for storing lists. See https://gitlab.com/listsync/\n\
            \n\
            List data is stored in the local directory, in the \
            file state.json.",
        )
        .arg(
            clap::Arg::with_name("bind")
                .long("bind")
                .value_name("ADDRESS")
                .help(
                    "Specify alternate bind address [default: all interfaces]",
                ),
        )
        .arg(
            clap::Arg::with_name("PORT")
                .index(1)
                .help("Specify alternate port to listen on [default: 8088]"),
        )
        .get_matches();

    Args {
        bind: String::from(matches.value_of("bind").unwrap_or("0.0.0.0")),
        port: String::from(matches.value_of("PORT").unwrap_or("8088")),
    }
}

fn cors() -> CorsFactory {
    Cors::new().max_age(3600).finish()
}

use actix_web::dev::Payload;
use actix_web::{FromRequest, HttpRequest};
use actix_web_httpauth::extractors::basic::{BasicAuth, Config};
use actix_web_httpauth::extractors::AuthenticationError;
use actix_web_httpauth::headers::www_authenticate::basic::Basic;
use futures::future::{FutureExt, Map, Ready};

pub struct LoggedInUser {
    pub username: String,
}

impl LoggedInUser {
    pub fn new(username: &str) -> LoggedInUser {
        LoggedInUser {
            username: String::from(username),
        }
    }
}

type ResBasic = Result<BasicAuth, AuthenticationError<Basic>>;

impl FromRequest for LoggedInUser {
    type Error = AuthenticationError<Basic>;
    type Future = Map<
        Ready<ResBasic>,
        Box<dyn FnOnce(ResBasic) -> Result<LoggedInUser, Self::Error>>,
    >;
    type Config = Config;

    fn from_request(
        req: &HttpRequest,
        payload: &mut Payload,
    ) -> <Self as FromRequest>::Future {
        BasicAuth::from_request(req, payload).map(Box::new(
            |res_basic: Result<BasicAuth, AuthenticationError<Basic>>| {
                res_basic.map(|basic| LoggedInUser::new(basic.user_id()))
            },
        ))
    }
}

use actix_web::dev::ServiceRequest;
use actix_web::error::ErrorUnauthorized;
use actix_web::web;
use actix_web::Error;
use actix_web_httpauth::extractors::basic::BasicAuth;

use crate::state::State;

pub async fn authenticate(
    req: ServiceRequest,
    basic_auth: BasicAuth,
) -> Result<ServiceRequest, Error> {
    let state: web::Data<State> = req.app_data().unwrap();
    let database = &state.database;
    valid_if(basic_auth, req, |username, password| {
        database.is_correct_token(username, password, state.clock.now())
            || database.is_correct_password(username, password)
    })
}

pub async fn authenticate_with_password(
    req: ServiceRequest,
    basic_auth: BasicAuth,
) -> Result<ServiceRequest, Error> {
    let state: web::Data<State> = req.app_data().unwrap();
    valid_if(basic_auth, req, |username, password| {
        state.database.is_correct_password(username, password)
    })
}

fn valid_if<F>(
    basic_auth: BasicAuth,
    req: ServiceRequest,
    f: F,
) -> Result<ServiceRequest, Error>
where
    F: FnOnce(&str, &str) -> bool,
{
    basic_auth
        .password()
        .ok_or(())
        .and_then(|password| {
            if f(basic_auth.user_id(), password) {
                Ok(())
            } else {
                Err(())
            }
        })
        .map(|_| req)
        .map_err(|_| ErrorUnauthorized("Invalid username or password"))
}

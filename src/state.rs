use crate::clock::Clock;
use crate::db::Database;
use crate::privacy_page::PrivacyPage;
use crate::token_generator::TokenGenerator;

pub struct State {
    pub database: Box<dyn Database>,
    pub token_generator: Box<dyn TokenGenerator>,
    pub clock: Box<dyn Clock>,
    pub privacy_page: PrivacyPage,
}

impl State {
    pub fn new(
        database: Box<dyn Database>,
        token_generator: Box<dyn TokenGenerator>,
        clock: Box<dyn Clock>,
        privacy_page: PrivacyPage,
    ) -> State {
        State {
            database,
            token_generator,
            clock,
            privacy_page,
        }
    }
}

use serde::{Deserialize, Serialize};

use crate::data::Error;

#[derive(Clone, Deserialize, Serialize)]
pub struct ListItem {
    pub itemid: String,
    pub order: f64,
    pub ticked: bool,
    pub text: String,
}

impl ListItem {
    pub fn get(&self, columnname: &str) -> Result<serde_json::Value, Error> {
        match columnname {
            "order" => serde_json::Number::from_f64(self.order)
                .map(|o| Ok(serde_json::Value::Number(o)))
                .unwrap_or(Err(Error::InternalError)),

            "ticked" => Ok(serde_json::Value::Bool(self.ticked)),
            "text" => Ok(serde_json::Value::String(self.text.clone())),
            _ => Err(Error::ColumnDoesNotExist),
        }
    }

    pub fn set(
        &mut self,
        columnname: &str,
        value: &serde_json::Value,
    ) -> Result<Self, Error> {
        match columnname {
            "order" => {
                if let serde_json::Value::Number(o) = value {
                    o.as_f64()
                        .map(|f| {
                            self.order = f;
                            Ok(self.clone())
                        })
                        .unwrap_or(Err(Error::WrongTypeForColumn(
                            "order", "f64",
                        )))
                } else {
                    Err(Error::WrongTypeForColumn("order", "f64"))
                }
            }
            "ticked" => {
                if let serde_json::Value::Bool(b) = value {
                    self.ticked = *b;
                    Ok(self.clone())
                } else {
                    Err(Error::WrongTypeForColumn("ticked", "bool"))
                }
            }
            "text" => {
                if let serde_json::Value::String(s) = value {
                    self.text = s.clone();
                    Ok(self.clone())
                } else {
                    Err(Error::WrongTypeForColumn("text", "string"))
                }
            }
            _ => Err(Error::ColumnDoesNotExist),
        }
    }
}

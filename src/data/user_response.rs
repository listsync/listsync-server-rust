use serde::Serialize;

#[derive(Serialize)]
pub struct UserResponse {
    pub username: String,
    pub admin: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub registration_code: Option<String>,
}

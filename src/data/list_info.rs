use serde::{Deserialize, Serialize};

#[derive(Clone, Deserialize, Serialize)]
pub struct ListInfo {
    pub listid: String,
    pub title: String,
}

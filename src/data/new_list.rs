use serde::Deserialize;

#[derive(Deserialize)]
pub struct NewList {
    pub title: String,
}

use serde::Deserialize;

#[derive(Deserialize)]
pub struct ReplacementItem {
    pub order: f64,
    pub ticked: bool,
    pub text: String,
}

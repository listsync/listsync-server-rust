use serde::Deserialize;

#[derive(Deserialize)]
pub struct UpdateUser {
    pub password: Option<String>,
    pub admin: Option<bool>,
}

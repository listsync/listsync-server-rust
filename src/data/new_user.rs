use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
pub struct NewUser {
    pub username: String,
    pub password: String,
    #[serde(default = "default_admin")]
    pub admin: bool,
}

fn default_admin() -> bool {
    false
}

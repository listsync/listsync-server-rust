use serde::Deserialize;

#[derive(Deserialize)]
pub struct NewItem {
    pub ticked: bool,
    pub text: String,
    pub order: Option<f64>,
    pub itemid: Option<String>,
}

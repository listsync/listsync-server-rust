use crate::data::Error;
use crate::logged_in_user::LoggedInUser;
use crate::state::State;

// Special case for bootstrapping - setup is allowed only
// if there are no users at all.
pub fn no_users_exist(state: &State) -> Result<(), Error> {
    if state.database.count_users() == 0 {
        Ok(())
    } else {
        Err(Error::SetupMayOnlyBeRunOnce)
    }
}

pub fn generate_token(
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    // All registered users are allowed to generate a login token
    allowed_if(is_registered(logged_in_user, state))
}

pub fn delete_token(
    token_owner_username: &String,
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    // Only you can log yourself out
    allowed_if(
        is_registered(logged_in_user, state)
            && logged_in_user.username == *token_owner_username,
    )
}

pub fn modify_set_of_users(
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    // Admins can change users list
    allowed_if(
        is_registered(logged_in_user, state) && is_admin(logged_in_user, state),
    )
}

pub fn modify_user(
    username_to_modify: &String,
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    // Admins and yourself can modify you
    allowed_if(
        is_registered(logged_in_user, state)
            && (logged_in_user.username == *username_to_modify
                || is_admin(logged_in_user, state)),
    )
}

pub fn modify_user_admin_status(
    _username_to_modify: &String,
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    // Only admins can modify admin status
    allowed_if(
        is_registered(logged_in_user, state) && is_admin(logged_in_user, state),
    )
}

pub fn delete_user(
    username_to_delete: &String,
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    // Admins and yourself can delete you
    allowed_if(
        is_registered(logged_in_user, state)
            && (logged_in_user.username == *username_to_delete
                || is_admin(logged_in_user, state)),
    )
}

pub fn read_user(
    username_to_read: &String,
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    // Admins and yourself can find out about you
    allowed_if(
        is_registered(logged_in_user, state)
            && (logged_in_user.username == *username_to_read
                || is_admin(logged_in_user, state)),
    )
}

pub fn read_set_of_lists(
    lists_owner_username: &str,
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    // You are allowed to list your own lists
    allowed_if(
        is_registered(logged_in_user, state)
            && logged_in_user.username == lists_owner_username,
    )
}

pub fn modify_set_of_lists(
    lists_owner_username: &str,
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    // You are allowed to modify your own lists
    allowed_if(
        is_registered(logged_in_user, state)
            && logged_in_user.username == lists_owner_username,
    )
}

pub fn delete_list(
    lists_owner_username: &str,
    _listid: &str,
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    // You are allowed to modify your own lists
    allowed_if(
        is_registered(logged_in_user, state)
            && logged_in_user.username == lists_owner_username,
    )
}

pub fn modify_list(
    lists_owner_username: &str,
    _listid: &str,
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    // You are allowed to modify your own list
    allowed_if(
        is_registered(logged_in_user, state)
            && logged_in_user.username == lists_owner_username,
    )
}

pub fn view_list(
    lists_owner_username: &str,
    _listid: &str,
    logged_in_user: &LoggedInUser,
    state: &State,
) -> Result<(), Error> {
    // You are allowed to view your own list
    allowed_if(
        is_registered(logged_in_user, state)
            && logged_in_user.username == lists_owner_username,
    )
}

fn allowed_if(condition: bool) -> Result<(), Error> {
    if condition {
        Ok(())
    } else {
        Err(Error::Unauthorized)
    }
}

fn is_admin(logged_in_user: &LoggedInUser, state: &State) -> bool {
    state
        .database
        .get_user(&logged_in_user.username)
        .map(|user_response| user_response.admin)
        .unwrap_or(false) // Any error means not an admin
}

fn is_registered(logged_in_user: &LoggedInUser, state: &State) -> bool {
    state
        .database
        .get_user(&logged_in_user.username)
        .map(|user_response| user_response.registration_code.is_none())
        .unwrap_or(false) // Any error means not registered
}

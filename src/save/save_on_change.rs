use std::sync::{Mutex, MutexGuard};

use crate::save::{SaveOnChangeGuard, SaveOnChangeMutGuard, Saver};

pub struct SaveOnChange<T> {
    mutex: Mutex<T>,
    saver: Box<dyn Saver<T>>,
}

impl<T> SaveOnChange<T> {
    pub fn new(t: T, saver: Box<dyn Saver<T>>) -> SaveOnChange<T> {
        SaveOnChange {
            mutex: Mutex::new(t),
            saver,
        }
    }
}

impl<T> SaveOnChange<T> {
    pub fn read_lock(&self) -> SaveOnChangeGuard<T> {
        SaveOnChangeGuard::new(self.mutex.lock().unwrap())
    }

    pub fn write_lock(&self) -> SaveOnChangeMutGuard<T> {
        SaveOnChangeMutGuard::new(self.mutex.lock().unwrap(), &*self.saver)
    }

    /// For special cases only: allow modifying the underlying
    /// data, but don't save it.  Use this only for non-critical
    /// modifications that may be ignored.
    /// For example, this is used when we are checking whether a
    /// token is valid.  We execute a side-effect of cleaning up
    /// unexpired tokens as we do this, but we don't want to
    /// trigger a save every time we check a token, and we know
    /// we can clean up again if this change is not saved.
    pub fn nonsaving_write_lock(&self) -> MutexGuard<T> {
        self.mutex.lock().unwrap()
    }
}

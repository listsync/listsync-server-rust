use std::ops::Deref;
use std::sync::MutexGuard;

pub struct SaveOnChangeGuard<'a, T> {
    mutex_guard: MutexGuard<'a, T>,
}

impl<'a, T> SaveOnChangeGuard<'a, T> {
    pub fn new(mutex_guard: MutexGuard<T>) -> SaveOnChangeGuard<T> {
        SaveOnChangeGuard { mutex_guard }
    }
}

impl<T> Deref for SaveOnChangeGuard<'_, T> {
    type Target = T;

    fn deref(&self) -> &T {
        self.mutex_guard.deref()
    }
}

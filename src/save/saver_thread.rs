use std::fs::File;
use std::io::Write;
use std::sync::mpsc::{channel, Receiver, Sender};
use std::thread;
use std::thread::JoinHandle;
use std::time::Duration;

const SAVE_INTERVAL: Duration = Duration::from_secs(20);

/**
 * Runs in a thread that is temporarily spawned
 * every time we have stuff to save.
 * Waits a while (to collect more changes)
 * then sends a message to the actual saver.
 */
fn wait_thread(sender: Sender<SaverThreadMessage>) {
    thread::sleep(SAVE_INTERVAL);
    sender.send(SaverThreadMessage::WaitThreadFinished).unwrap();
}

pub enum SaverThreadMessage {
    InfoToSave(String),
    WaitThreadFinished,
    Quit,
}

pub struct SaverThread {
    receiver: Receiver<SaverThreadMessage>,
    sender: Sender<SaverThreadMessage>,
    filename: String,
    info_to_save: Option<String>,
    wait_thread_running: bool,
}

impl SaverThread {
    pub fn spawn(
        filename: String,
    ) -> (Sender<SaverThreadMessage>, JoinHandle<()>) {
        let (sender, receiver) = channel();
        let sender_clone = sender.clone();
        let join_handle = thread::spawn(move || {
            SaverThread::new(filename, sender_clone, receiver).run()
        });
        (sender, join_handle)
    }

    fn new(
        filename: String,
        sender: Sender<SaverThreadMessage>,
        receiver: Receiver<SaverThreadMessage>,
    ) -> SaverThread {
        SaverThread {
            receiver,
            sender,
            filename,
            info_to_save: None,
            wait_thread_running: false,
        }
    }

    fn run(&mut self) {
        loop {
            match self.receiver.recv().unwrap() {
                SaverThreadMessage::InfoToSave(s) => {
                    self.info_to_save = Some(s);
                    if !self.wait_thread_running {
                        let sender_clone = self.sender.clone();
                        thread::spawn(move || wait_thread(sender_clone));
                        self.wait_thread_running = true;
                    }
                }
                SaverThreadMessage::WaitThreadFinished => {
                    self.wait_thread_running = false;
                    self.try_save();
                }
                SaverThreadMessage::Quit => {
                    self.try_save();
                    break;
                }
            }
        }
    }

    fn try_save(&mut self) {
        if let Some(str_to_save) = &self.info_to_save {
            match self.save(str_to_save) {
                Err(e) => eprintln!("ERROR SAVING {}", e),
                Ok(_) => {
                    println!("Saved successfully.");
                    self.info_to_save = None;
                }
            }
        } else {
            println!("Nothing to save.");
        }
    }

    fn new_filename(&self) -> String {
        format!("{}.new", self.filename)
    }

    fn save(&self, to_save: &str) -> std::io::Result<()> {
        {
            let mut f = File::create(self.new_filename())?;
            f.write_all(to_save.as_bytes())?;
        }
        std::fs::rename(self.new_filename(), &self.filename)?;
        Ok(())
    }
}

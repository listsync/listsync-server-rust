use rand::{thread_rng, Rng};

use crate::idgen::IdGen;

pub struct RealIdGen {}

impl RealIdGen {
    pub fn new() -> RealIdGen {
        RealIdGen {}
    }
}

impl IdGen for RealIdGen {
    fn gen(&self) -> String {
        base64::encode_config(
            thread_rng().gen::<[u8; 3]>(),
            base64::URL_SAFE_NO_PAD,
        )
    }
}

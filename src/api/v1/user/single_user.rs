use actix_web::{web, HttpResponse, Responder};
use serde::Deserialize;

use crate::authorization;
use crate::data::UpdateUser;
use crate::logged_in_user::LoggedInUser;
use crate::response;
use crate::state::State;

#[derive(Deserialize)]
struct Path {
    username: String,
}

async fn get(
    path: web::Path<Path>,
    state: web::Data<State>,
    logged_in_user: LoggedInUser,
) -> impl Responder {
    authorization::read_user(&path.username, &logged_in_user, &state)
        .and_then(|_| state.database.get_user(&path.username))
        .map(|user_response| HttpResponse::Ok().json(user_response))
        .map_err(response::from_error)
}

async fn put(
    path: web::Path<Path>,
    update_user: web::Json<UpdateUser>,
    state: web::Data<State>,
    logged_in_user: LoggedInUser,
) -> impl Responder {
    let auth = if update_user.admin == None {
        authorization::modify_user(&path.username, &logged_in_user, &state)
    } else {
        authorization::modify_user_admin_status(
            &path.username,
            &logged_in_user,
            &state,
        )
    };

    auth.and_then(|_| state.database.update_user(&path.username, &update_user))
        .map(|user_response| HttpResponse::Ok().json(user_response))
        .map_err(response::from_error)
}

async fn delete(
    path: web::Path<Path>,
    state: web::Data<State>,
    logged_in_user: LoggedInUser,
) -> impl Responder {
    authorization::delete_user(&path.username, &logged_in_user, &state)
        .and_then(|_| state.database.delete_user(&path.username))
        .map(|_| HttpResponse::NoContent())
        .map_err(response::from_error)
}

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::resource("/{username}")
            .route(web::get().to(get))
            .route(web::put().to(put))
            .route(web::delete().to(delete)),
    );
}

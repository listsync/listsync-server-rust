use actix_web::{web, HttpResponse, Responder};

use crate::authorization;
use crate::data::NewUser;
use crate::logged_in_user::LoggedInUser;
use crate::response;
use crate::state::State;

async fn post(
    new_user: web::Json<NewUser>,
    state: web::Data<State>,
    logged_in_user: LoggedInUser,
) -> impl Responder {
    authorization::modify_set_of_users(&logged_in_user, &state)
        .and_then(|_| state.database.add_user(&new_user, None))
        .map(|user_response| HttpResponse::Created().json(user_response))
        .map_err(response::from_error)
}

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(web::resource("").route(web::post().to(post)));
}

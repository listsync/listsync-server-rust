use actix_web::{web, HttpResponse, Responder};

use crate::state::State;

async fn get(state: web::Data<State>) -> impl Responder {
    // Note: no authorization!
    HttpResponse::Ok().json(serde_json::Value::String(String::from(
        &state.privacy_page.contents,
    )))
}

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(web::resource("").route(web::get().to(get)));
}

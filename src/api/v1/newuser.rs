use actix_web::{web, HttpResponse, Responder};
use serde::{Deserialize, Serialize};

use crate::data::{Error, NewUser};
use crate::response;
use crate::state::State;

#[derive(Deserialize, Serialize)]
pub struct NewUnregisteredUser {
    pub username: String,
    pub password: String,
}

#[derive(Deserialize, Serialize)]
struct RegistrationCode {
    registration_code: String,
}

#[derive(Deserialize)]
struct Path {
    username: String,
}

async fn post(
    new_user: web::Json<NewUnregisteredUser>,
    state: web::Data<State>,
) -> impl Responder {
    // Note: no authorization!
    state
        .database
        .add_user(
            &NewUser {
                username: String::from(&new_user.username),
                password: String::from(&new_user.password),
                admin: false,
            },
            Some(state.token_generator.generate_registration_code()),
        )
        .map(|user_response| HttpResponse::Created().json(user_response))
        .map_err(response::from_error)
}

async fn get(path: web::Path<Path>, state: web::Data<State>) -> impl Responder {
    // Note: no authorization!
    state
        .database
        .get_user(&path.username)
        .and_then(|user_response| {
            user_response
                .registration_code
                .ok_or(Error::UserIsAlreadyRegistered)
        })
        .map(|registration_code| {
            HttpResponse::Ok().json(RegistrationCode { registration_code })
        })
        .map_err(response::from_error)
}

async fn put(
    path: web::Path<Path>,
    registration_code: web::Json<RegistrationCode>,
    state: web::Data<State>,
) -> impl Responder {
    // Note: no authorization!
    let user_code =
        state
            .database
            .get_user(&path.username)
            .and_then(|user_response| {
                user_response
                    .registration_code
                    .ok_or(Error::UserIsAlreadyRegistered)
            });

    user_code
        .and_then(|user_code| {
            if user_code == registration_code.registration_code {
                state
                    .database
                    .set_user_registered(&path.username)
                    .map(|_| HttpResponse::NoContent())
            } else {
                Err(Error::IncorrectRegistrationCode)
            }
        })
        .map_err(response::from_error)
}
pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(web::resource("").route(web::post().to(post)))
        .service(
            web::resource("{username}")
                .route(web::get().to(get))
                .route(web::put().to(put)),
        );
}

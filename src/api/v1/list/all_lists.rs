use actix_web::{web, HttpResponse, Responder};
use serde::Deserialize;

use crate::authorization;
use crate::data::NewList;
use crate::logged_in_user::LoggedInUser;
use crate::response;
use crate::state::State;

#[derive(Deserialize)]
struct Path {
    username: String,
}

async fn get(
    path: web::Path<Path>,
    state: web::Data<State>,
    logged_in_user: LoggedInUser,
) -> impl Responder {
    authorization::read_set_of_lists(&path.username, &logged_in_user, &state)
        .and_then(|_| state.database.get_lists(&path.username))
        .map(|lists| HttpResponse::Ok().json(lists))
        .map_err(response::from_error)
}

async fn post(
    path: web::Path<Path>,
    new_list: web::Json<NewList>,
    state: web::Data<State>,
    logged_in_user: LoggedInUser,
) -> impl Responder {
    authorization::modify_set_of_lists(&path.username, &logged_in_user, &state)
        .and_then(|_| state.database.add_list(&path.username, &new_list.title))
        .map(|list_info| HttpResponse::Created().json(list_info))
        .map_err(response::from_error)
}

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::resource("/{username}")
            .route(web::get().to(get))
            .route(web::post().to(post)),
    );
}

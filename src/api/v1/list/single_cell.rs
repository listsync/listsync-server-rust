use actix_web::{web, HttpResponse, Responder};
use serde::Deserialize;

use crate::authorization;
use crate::logged_in_user::LoggedInUser;
use crate::response;
use crate::state::State;

#[derive(Deserialize)]
struct Path {
    username: String,
    listid: String,
    itemid: String,
    columname: String,
}

async fn get(
    path: web::Path<Path>,
    state: web::Data<State>,
    logged_in_user: LoggedInUser,
) -> impl Responder {
    authorization::view_list(
        &path.username,
        &path.listid,
        &logged_in_user,
        &state,
    )
    .and_then(|_| {
        state.database.get_cell(
            &path.username,
            &path.listid,
            &path.itemid,
            &path.columname,
        )
    })
    .map(|value| HttpResponse::Ok().json(value))
    .map_err(response::from_error)
}

async fn put(
    path: web::Path<Path>,
    cell: web::Json<serde_json::Value>,
    state: web::Data<State>,
    logged_in_user: LoggedInUser,
) -> impl Responder {
    authorization::modify_list(
        &path.username,
        &path.listid,
        &logged_in_user,
        &state,
    )
    .and_then(|_| {
        state.database.set_cell(
            &path.username,
            &path.listid,
            &path.itemid,
            &path.columname,
            &cell,
        )
    })
    .map(|_| HttpResponse::NoContent())
    .map_err(response::from_error)
}

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::resource("/{username}/{listid}/items/{itemid}/{columname}")
            .route(web::put().to(put))
            .route(web::get().to(get)),
    );
}

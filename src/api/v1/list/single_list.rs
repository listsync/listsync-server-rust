use actix_web::{web, HttpResponse, Responder};
use serde::Deserialize;

use crate::authorization;
use crate::data::{NewItem, NewList};
use crate::logged_in_user::LoggedInUser;
use crate::response;
use crate::state::State;

#[derive(Deserialize)]
struct Path {
    username: String,
    listid: String,
}

async fn get(
    path: web::Path<Path>,
    state: web::Data<State>,
    logged_in_user: LoggedInUser,
) -> impl Responder {
    authorization::view_list(
        &path.username,
        &path.listid,
        &logged_in_user,
        &state,
    )
    .and_then(|_| state.database.get_list(&path.username, &path.listid))
    .map(|list_info| HttpResponse::Ok().json(list_info))
    .map_err(response::from_error)
}

async fn put(
    path: web::Path<Path>,
    updated_list: web::Json<NewList>,
    state: web::Data<State>,
    logged_in_user: LoggedInUser,
) -> impl Responder {
    authorization::modify_list(
        &path.username,
        &path.listid,
        &logged_in_user,
        &state,
    )
    .and_then(|_| {
        state.database.update_list(
            &path.username,
            &path.listid,
            &updated_list.title,
        )
    })
    .map(|list_info| HttpResponse::Ok().json(list_info))
    .map_err(response::from_error)
}

async fn delete(
    path: web::Path<Path>,
    state: web::Data<State>,
    logged_in_user: LoggedInUser,
) -> impl Responder {
    authorization::delete_list(
        &path.username,
        &path.listid,
        &logged_in_user,
        &state,
    )
    .and_then(|_| state.database.delete_list(&path.username, &path.listid))
    .map(|_| HttpResponse::NoContent())
    .map_err(response::from_error)
}

async fn post_item(
    path: web::Path<Path>,
    new_item: web::Json<NewItem>,
    state: web::Data<State>,
    logged_in_user: LoggedInUser,
) -> impl Responder {
    authorization::modify_list(
        &path.username,
        &path.listid,
        &logged_in_user,
        &state,
    )
    .and_then(|_| {
        state
            .database
            .add_item(&path.username, &path.listid, &new_item)
    })
    .map(|list_info| HttpResponse::Created().json(list_info))
    .map_err(response::from_error)
}

async fn put_items(
    path: web::Path<Path>,
    new_items: web::Json<Vec<NewItem>>,
    state: web::Data<State>,
    logged_in_user: LoggedInUser,
) -> impl Responder {
    authorization::modify_list(
        &path.username,
        &path.listid,
        &logged_in_user,
        &state,
    )
    .and_then(|_| {
        state
            .database
            .set_items(&path.username, &path.listid, &new_items)
    })
    .map(|items| HttpResponse::Ok().json(items))
    .map_err(response::from_error)
}

async fn get_items(
    path: web::Path<Path>,
    state: web::Data<State>,
    logged_in_user: LoggedInUser,
) -> impl Responder {
    authorization::view_list(
        &path.username,
        &path.listid,
        &logged_in_user,
        &state,
    )
    .and_then(|_| state.database.get_items(&path.username, &path.listid))
    .map(|items| HttpResponse::Ok().json(items))
    .map_err(response::from_error)
}

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::resource("/{username}/{listid}")
            .route(web::get().to(get))
            .route(web::put().to(put))
            .route(web::delete().to(delete)),
    )
    .service(
        web::resource("/{username}/{listid}/items")
            .route(web::post().to(post_item))
            .route(web::get().to(get_items))
            .route(web::put().to(put_items)),
    );
}

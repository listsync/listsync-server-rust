pub trait IdGen: Send + Sync {
    fn gen(&self) -> String;
}

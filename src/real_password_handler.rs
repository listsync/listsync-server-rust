use rand::Rng;

use crate::password_handler::PasswordHandler;

pub struct RealPasswordHandler {}

impl PasswordHandler for RealPasswordHandler {
    fn hash_password(&self, password: &str) -> String {
        argon2::hash_encoded(
            password.as_bytes(),
            &rand::thread_rng().gen::<[u8; 32]>(),
            &argon2::Config::default(),
        )
        .unwrap()
    }

    fn is_correct_password(
        &self,
        possible_password: &str,
        user_hashed_password: &str,
    ) -> bool {
        argon2::verify_encoded(
            user_hashed_password,
            possible_password.as_bytes(),
        )
        .unwrap()
    }
}

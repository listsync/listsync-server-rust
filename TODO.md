# TODO

## Basic noscript web UI

Server items:

- [x] Token-based auth
- [x] Hash the auth tokens on the server
- [x] List IDs instead of listnames
- [x] Rename list via PUT
- [x] Data backup and deletion in line with privacy policy
- [x] Allow deleting auth tokens
- [x] User registration
- [x] Privacy policy
- [x] Change password
- [x] Reduce length of security token to 48 bytes
- [ ] Reset guest account hourly

See [HTML client
TODO](https://gitlab.com/listsync/listsync-client-rust/-/blob/master/TODO.md).

## Finishing

- [ ] Public web site at listsync.artificialworlds.net
- [ ] Make Database async
- [ ] Disallow duplicate item IDs
- [ ] Allow updating a user (to make them admin)
- [ ] Automatic conversion from Error to HttpResponse
- [ ] Automatic conversion from e.g. Lists to HttpResponse
- [ ] Return JSON on invalid username/password
- [ ] Check all TODOs and do them
- [ ] Restrict listnames to url-friendly characters, and add friendly names
- [ ] Restrict usernames to url-friendly characters, and add friendly names

## Single-page web UI

- Probably based on Elm TODOMVC
- Look at CSS animations e.g. https://codepen.io/milanraring/pen/QWbqBGo

## Android app (widget)

Do as much as possible in the widget itself.

## Production ready

- [ ] Allow modifying users with PUT.  (Prevent users promoting
      themselves to admin.)
- [ ] Monitoring and logging
- [ ] Security limits e.g. blocking after too many login attempts
- [ ] Scalable data storage e.g. MariaDB, MongoDB
- [ ] Automated data backup and deletion in line with privacy policy
- [ ] Stop returning registration code over HTTP - use email?

## Later

- Share lists (view-only and editable)

# Done

## API kinda exists (v0.1.0)

- [x] Create/update Users, lists, items
- [x] Save state to a file
- [x] A way to bootstrap a system with no users
- [x] HTTP Basic Authentication
- [x] Authorization and admin users
- [x] Speed up tests by not hashing passwords
- [x] Restructure to a Database interface
- [x] Rename credentials to logged_in_user
- [x] Rename listslug to listname
- [x] Rename auth to authentication
- [x] Consider not wrapping LoggedInUser properties with functions
- [x] Consider moving UserInfo into db/memory_database/
- [x] Move State out of data
- [x] Consider passing a PasswordHandler into MemoryDatabase
- [x] Delete users, lists, items
- [x] Allow updating "order" with PUT
- [x] Way of asking whether we are in setup mode
- [x] Deploy to my webspace using Apache to provide HTTPS
- [x] OpenAPI documentation
- [x] CORS support
- [x] Restrict so just /api requests go to the server
- [x] Support leading // to make proxying work


# listsync Server

*Not actively maintained.*

A RESTful server that allows you to add, update and query lists.

## Prerequisites

[Install Rust](https://www.rust-lang.org/tools/install).

## Build

```bash
cargo test
```

## Run

```bash
cargo run
```

To change the interface or port to bind on, use something like:

```bash
cargo run -- --bind 127.0.0.1 8000
```

## API docs

See the interactive documentation (Swagger) at
[listsync.gitlab.io/listsync-spec](https://listsync.gitlab.io/listsync-spec).

## Usage

See "Run" above for how to run the server locally.  Once it's running, you can
make requests as shown below.

Create the first user via the special `setup` path:

```bash
curl -v http://localhost:8088/v1/setup -H 'Content-Type: application/json' -d '{"username":"andy", "password":"pw"}'
```

Create a list:

```bash
curl -v -uandy:pw http://localhost:8088/v1/list/andy -H 'Content-Type: application/json' -d '{"title":"mylist"}'
{"listid":"abcd","title":mylist"}
```

Add an item:

```bash
curl -v -uandy:pw http://localhost:8088/v1/list/andy/abcd/items -H 'Content-Type: application/json' -d '{"ticked":false, "text": "Buy oranges"}'
```

Update an item:

```bash
curl -v -uandy:pw -X PUT http://localhost:8088/v1/list/andy/abcd/items/0001/ticked -H 'Content-Type: application/json' -d 'true'
```

View list items:

```bash
curl -uandy:pw http://localhost:8088/v1/list/andy/abcd/items
```

Create a new user:

```bash
curl -v -uandy:pw http://localhost:8088/v1/user -H 'Content-Type: application/json' -d '{"username":"bridget", "password":"secret"}'
```

## Security tokens

To avoid sending the password every time, you can create a time-limited security token:

```bash
curl -uandy:pw http://localhost:8088/v1/login -H 'Content-Type: application/json' -d '{"remember_me":false}'
```

This will provide a response like this:

```
{"username":"andy","token":"zPntVAgzeyO5niu20boEbINxUvs49d38N59ijSLNDYftQMC7Pi/bTFBSgknL62h20R7Bvkk/hFqXrtm8nKV7OBlZHgJWkojWr/uFcVovWX8rIqZmzvnx0IhQEMrJFBUd+R/GHq9M5vH8QxtisDEgtqIQH5sY+XKCc+FVDFOroso="}
```

Now you can use the supplied token in place of the password:

```bash
curl '-uandy:zPntVAgzeyO5niu20boEbINxUvs49d38N59ijSLNDYftQMC7Pi/bTFBSgknL62h20R7Bvkk/hFqXrtm8nKV7OBlZHgJWkojWr/uFcVovWX8rIqZmzvnx0IhQEMrJFBUd+R/GHq9M5vH8QxtisDEgtqIQH5sY+XKCc+FVDFOroso=' http://localhost:8088/v1/list/andy/abcd/items
```

This token will expire in 24 hours, or if you supply `{"remember_me":true}` in
the login request, it will expire in 21 days.

## listsync Spec

This server implements the API specified at
[listsync-spec](https://gitlab.com/listsync/listsync-spec).

## Clients

If you want to actually edit lists, you will need a client that can connect
to this server.  Try
[listsync-client-rust](https://gitlab.com/listsync/listsync-client-rust).

## License

Copyright 2019-2020 Andy Balaam.

Released under the GNU AGPLv3, or later.  See [LICENSE](LICENSE).

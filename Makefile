all:
	cargo fmt
	cargo test

run:
	cargo run

DEPL := listsync_webspace:listsync-server-rust

deploy:
	cargo build --release
	scp target/release/listsync-server-rust ${DEPL}/listsync-server-rust.new
	scp privacy-artificialworlds.html ${DEPL}/privacy.html

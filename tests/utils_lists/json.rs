use actix_web::web::Bytes;
use serde_json;

fn json_type(value: &serde_json::Value) -> &'static str {
    match *value {
        serde_json::Value::Array(_) => "array",
        serde_json::Value::Bool(_) => "bool",
        serde_json::Value::Null => "null",
        serde_json::Value::Number(_) => "number",
        serde_json::Value::Object(_) => "object",
        serde_json::Value::String(_) => "string",
    }
}

pub fn parse(resp: &Bytes) -> serde_json::Value {
    serde_json::from_slice(&resp[..]).expect(&format!(
        "Response is not json: {}",
        String::from_utf8(resp.to_vec()).expect("Response is not utf8.")
    ))
}

fn assert_type(resp: &serde_json::Value, key: &str, expected_type: &str) {
    if json_type(&resp[key]) != expected_type {
        panic!(
            "In '{}' we expected the '{}' property \
             to be a {} but it was '{}'.",
            resp, key, expected_type, resp[key]
        )
    }
}

fn assert_prop_eq(
    resp: &serde_json::Value,
    key: &str,
    exp_prop: &serde_json::Value,
) {
    if resp[key] != *exp_prop {
        panic!(
            "In '{}' we expected the '{}' property \
             to be '{}' but it was '{}'.",
            resp, key, exp_prop, resp[key]
        )
    }
}

pub fn assert_matches(resp: &serde_json::Value, expected: serde_json::Value) {
    if let serde_json::Value::Object(_) = resp {
        if let serde_json::Value::Object(exp) = &expected {
            for (key, value) in exp {
                if let serde_json::Value::String(s) = &value {
                    match &s as &str {
                        "<array>" => assert_type(resp, &key, "array"),
                        "<bool>" => assert_type(resp, &key, "bool"),
                        "<number>" => assert_type(resp, &key, "number"),
                        "<object>" => assert_type(resp, &key, "object"),
                        "<string>" => assert_type(resp, &key, "string"),
                        _ => assert_prop_eq(resp, &key, &value),
                    }
                } else {
                    assert_prop_eq(resp, &key, &value)
                }
            }
        } else {
            panic!("expected must be an Object");
        }
    } else {
        panic!("Can only use assert_matches on Objects")
    }
}

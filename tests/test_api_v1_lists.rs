extern crate listsync_server_rust;

mod utils;
mod utils_lists;

use utils::log_in_as::LogInAs;
use utils::new_app::{new_app, new_app_counting_saves, FakeSaver};
use utils::requests::{delete, get, get_nou, post, post_nou, put, test_req};
use utils_lists::json::{assert_matches, parse};
use utils_lists::requests_lists::get_f64;

const USRA: LogInAs = LogInAs {
    username: "auser",
    password: "apass",
};

#[test]
fn admin_attempting_to_get_a_nonexistent_users_lists_is_forbidden() {
    let saver = FakeSaver::new();
    let mut app = new_app_counting_saves(saver.clone());
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    test_req(
        get(&mut app, USRA, "/v1/list/nonexistentuser"),
        r#"403 {"error":"Permission denied."}"#,
    );
    assert_eq!(saver.num_saves(), 1);
}

#[test]
fn nonadmin_attempting_to_get_a_nonexistent_users_lists_is_forbidden() {
    let nonadm = LogInAs {
        username: "normal",
        password: "np",
    };

    let saver = FakeSaver::new();
    let mut app = new_app_counting_saves(saver.clone());
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    test_req(
        post(&mut app, USRA, "/v1/user", &nonadm.creation_json()),
        r#"201 {"username":"normal","admin":false}"#,
    );
    test_req(
        get(&mut app, nonadm, "/v1/list/nonexistentuser"),
        r#"403 {"error":"Permission denied."}"#,
    );
}

#[test]
fn admin_attempting_to_get_a_other_users_lists_is_forbidden() {
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    post(
        &mut app,
        USRA,
        "/v1/user",
        &LogInAs {
            username: "ali",
            password: "p",
        }
        .creation_json(),
    );
    test_req(
        get(&mut app, USRA, "/v1/list/ali"),
        r#"403 {"error":"Permission denied."}"#,
    );
}

#[test]
fn new_user_has_no_lists() {
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    test_req(get(&mut app, USRA, "/v1/list/auser"), r#"200 []"#);
}

#[test]
fn looking_at_my_lists_with_incorrect_password_is_forbidden() {
    let mut app = new_app();
    test_req(
        post_nou(&mut app, "/v1/setup", &USRA.creation_json()),
        r#"201 {"username":"auser","admin":true}"#,
    );
    let wrong_password = LogInAs {
        username: "auser",
        password: "WRONG",
    };

    test_req(
        get(&mut app, wrong_password, "/v1/list/auser"),
        r#"401 Invalid username or password"#,
    );
}

#[test]
fn looking_at_my_lists_with_no_auth_header_is_forbidden() {
    let mut app = new_app();
    test_req(
        post_nou(&mut app, "/v1/setup", &USRA.creation_json()),
        r#"201 {"username":"auser","admin":true}"#,
    );

    // Don't supply a username or password at all - should fail
    test_req(get_nou(&mut app, "/v1/list/auser"), r#"401 "#);
}

#[test]
fn a_newly_added_list_is_listed() {
    let saver = FakeSaver::new();
    let mut app = new_app_counting_saves(saver.clone());
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    test_req(
        post(&mut app, USRA, "/v1/list/auser", r#"{"title": "mylist"}"#),
        r#"201 {"listid":"id1","title":"mylist"}"#,
    );
    assert_eq!(saver.num_saves(), 2);
    test_req(
        get(&mut app, USRA, "/v1/list/auser"),
        r#"200 [{"listid":"id1","title":"mylist"}]"#,
    );
    assert_eq!(saver.num_saves(), 2);
}

#[test]
fn a_newly_added_list_can_be_retrieved() {
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    post(&mut app, USRA, "/v1/list/auser", r#"{"title": "mylist"}"#);
    test_req(
        get(&mut app, USRA, "/v1/list/auser/id1"),
        r#"200 {"listid":"id1","title":"mylist"}"#,
    );
}

#[test]
fn a_list_with_empty_title_can_be_retrieved() {
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    post(&mut app, USRA, "/v1/list/auser", r#"{"title": ""}"#);
    test_req(
        get(&mut app, USRA, "/v1/list/auser/id1"),
        r#"200 {"listid":"id1","title":""}"#,
    );
}

#[test]
fn a_list_can_be_renamed() {
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    post(&mut app, USRA, "/v1/list/auser", r#"{"title": "old"}"#);
    test_req(
        put(&mut app, USRA, "/v1/list/auser/id1", r#"{"title": "new"}"#),
        r#"200 {"listid":"id1","title":"new"}"#,
    );
    test_req(
        get(&mut app, USRA, "/v1/list/auser/id1"),
        r#"200 {"listid":"id1","title":"new"}"#,
    );
}

#[test]
fn a_user_can_have_multiple_lists() {
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    test_req(
        post(&mut app, USRA, "/v1/list/auser", r#"{"title":"list1"}"#),
        r#"201 {"listid":"id1","title":"list1"}"#,
    );
    test_req(
        post(&mut app, USRA, "/v1/list/auser", r#"{"title":"list2"}"#),
        r#"201 {"listid":"id2","title":"list2"}"#,
    );
    test_req(
        get(&mut app, USRA, "/v1/list/auser"),
        &format!(
            "200 [{},{}]",
            r#"{"listid":"id1","title":"list1"}"#,
            r#"{"listid":"id2","title":"list2"}"#,
        ),
    );
}

#[test]
fn fetching_a_nonexistent_list_is_an_error() {
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    post(&mut app, USRA, "/v1/list/auser", r#"{"listid": "mylist"}"#);
    test_req(
        get(&mut app, USRA, "/v1/list/auser/nonexistentlist"),
        r#"404 {"error":"List does not exist."}"#,
    );
}

#[test]
fn adding_an_item_returns_its_id_and_order() {
    let saver = FakeSaver::new();
    let mut app = new_app_counting_saves(saver.clone());
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    post(&mut app, USRA, "/v1/list/auser", r#"{"title": "mylist"}"#);
    let (status, resp) = post(
        &mut app,
        USRA,
        "/v1/list/auser/id1/items",
        r#"{"ticked": false, "text": "My Item"}"#,
    );
    assert_eq!(status, 201);
    assert_matches(
        &parse(&resp),
        serde_json::json!(
            { "ticked": false
            , "text": "My Item"
            , "order": "<number>"
            , "itemid": "<string>"
            }
        ),
    );
    assert_eq!(saver.num_saves(), 3);
}

#[test]
fn added_item_is_returned() {
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    post(&mut app, USRA, "/v1/list/auser", r#"{"title": "mylist"}"#);
    let (_, add_resp) = post(
        &mut app,
        USRA,
        "/v1/list/auser/id1/items",
        r#"{"ticked": false, "text": "My Item"}"#,
    );
    let add_resp = parse(&add_resp);
    let add_id = &add_resp["itemid"];
    let add_order = &add_resp["order"];

    let (status, resp) = get(&mut app, USRA, "/v1/list/auser/id1/items");
    assert_eq!(status, 200);
    let item0 = &parse(&resp)[0];
    assert_matches(
        item0,
        serde_json::json!(
            { "ticked": false
            , "text": "My Item"
            , "order": "<number>"
            , "itemid": "<string>"
            }
        ),
    );

    assert_eq!(item0["itemid"], *add_id);
    assert_eq!(item0["order"], *add_order);
}

#[test]
fn adding_an_item_with_order_allows_inserting_it() {
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    post(&mut app, USRA, "/v1/list/auser", r#"{"title": "mylist"}"#);
    post(
        &mut app,
        USRA,
        "/v1/list/auser/id1/items",
        r#"{"ticked": false, "text": "I16"}"#,
    );
    post(
        &mut app,
        USRA,
        "/v1/list/auser/id1/items",
        r#"{"ticked": false, "text": "I8", "order": 8.0}"#,
    );

    assert_eq!(
        parse(&get(&mut app, USRA, "/v1/list/auser/id1/items").1),
        serde_json::json!(
            [
                { "ticked": false
                , "text": "I8"
                , "order": 8.0
                , "itemid": "0002"
                },
                { "ticked": false
                , "text": "I16"
                , "order": 16.0
                , "itemid": "0001"
                },
            ]
        ),
    )
}

#[test]
fn multiple_added_items_are_returned() {
    let saver = FakeSaver::new();
    let mut app = new_app_counting_saves(saver.clone());
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    post(&mut app, USRA, "/v1/list/auser", r#"{"title": "mylist"}"#);
    post(
        &mut app,
        USRA,
        "/v1/list/auser/id1/items",
        r#"{"ticked": false, "text": "Item Z"}"#,
    );
    post(
        &mut app,
        USRA,
        "/v1/list/auser/id1/items",
        r#"{"ticked": true, "text": "Item A"}"#,
    );
    post(
        &mut app,
        USRA,
        "/v1/list/auser/id1/items",
        r#"{"ticked": true, "text": "Item B"}"#,
    );
    assert_eq!(saver.num_saves(), 5);

    let (_, resp) = get(&mut app, USRA, "/v1/list/auser/id1/items");
    let item0 = &parse(&resp)[0];
    let item1 = &parse(&resp)[1];
    let item2 = &parse(&resp)[2];
    assert_eq!(item0["text"].as_str().unwrap(), "Item Z");
    assert_eq!(item1["text"].as_str().unwrap(), "Item A");
    assert_eq!(item2["text"].as_str().unwrap(), "Item B");
    assert_eq!(item0["ticked"].as_bool().unwrap(), false);
    assert_eq!(item1["ticked"].as_bool().unwrap(), true);
    assert_eq!(item2["ticked"].as_bool().unwrap(), true);
    assert_ne!(item0["itemid"], item1["itemid"]);
    assert_ne!(item1["itemid"], item2["itemid"]);
    assert_ne!(item0["itemid"], item2["itemid"]);
    assert!(
        item0["order"].as_f64().unwrap() < item1["order"].as_f64().unwrap()
    );
    assert!(
        item1["order"].as_f64().unwrap() < item2["order"].as_f64().unwrap()
    );
    assert_eq!(saver.num_saves(), 5);
}

#[test]
fn can_replace_an_items_properties() {
    let saver = FakeSaver::new();
    let mut app = new_app_counting_saves(saver.clone());
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    post(&mut app, USRA, "/v1/list/auser", r#"{"title": "mylist"}"#);
    let (_, add_resp) = post(
        &mut app,
        USRA,
        "/v1/list/auser/id1/items",
        r#"{"ticked": false, "text": "My Item"}"#,
    );
    let add_resp = parse(&add_resp);
    let add_id = add_resp["itemid"].as_str().unwrap();
    let add_order = add_resp["order"].as_f64().unwrap();

    assert_eq!(saver.num_saves(), 3);
    let (status, resp) = put(
        &mut app,
        USRA,
        &format!("/v1/list/auser/id1/items/{}", add_id),
        &serde_json::json!(
            {"order": add_order, "ticked": true, "text": "Updated"}
        )
        .to_string(),
    );
    assert_eq!(status, 200);
    assert_eq!(saver.num_saves(), 4);
    assert_eq!(
        parse(&resp),
        serde_json::json!(
            { "ticked": true
            , "text": "Updated"
            , "order": add_order
            , "itemid": add_id
            }
        ),
    );

    let (status, resp) = get(
        &mut app,
        USRA,
        &format!("/v1/list/auser/id1/items/{}", add_id),
    );
    assert_eq!(status, 200);
    assert_eq!(
        parse(&resp),
        serde_json::json!(
            { "ticked": true
            , "text": "Updated"
            , "order": add_order
            , "itemid": add_id
            }
        ),
    );
    assert_eq!(saver.num_saves(), 4);
}

#[test]
fn changing_an_item_order_reorders_the_list() {
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    post(&mut app, USRA, "/v1/list/auser", r#"{"title": "mylist"}"#);

    // Add 3 items
    let items_url = "/v1/list/auser/id1/items";
    post(
        &mut app,
        USRA,
        items_url,
        r#"{"ticked": true, "text": "I1"}"#,
    );
    post(
        &mut app,
        USRA,
        items_url,
        r#"{"ticked": true, "text": "I2"}"#,
    );
    post(
        &mut app,
        USRA,
        items_url,
        r#"{"ticked": true, "text": "I3"}"#,
    );

    // Get them again
    let resp = parse(&get(&mut app, USRA, items_url).1);
    let new_order = (resp[0]["order"].as_f64().unwrap()
        + resp[1]["order"].as_f64().unwrap())
        / 2.0;

    // Update the third one to come second
    put(
        &mut app,
        USRA,
        &format!(
            "/v1/list/auser/id1/items/{}",
            resp[2]["itemid"].as_str().unwrap()
        ),
        &serde_json::json!(
            {"order": new_order, "ticked": true, "text": "I3a"}
        )
        .to_string(),
    );

    // Check they were reordered.
    let all_items = parse(&get(&mut app, USRA, items_url).1);

    let texts: Vec<&str> = all_items
        .as_array()
        .unwrap()
        .iter()
        .map(|item| item["text"].as_str().unwrap())
        .collect();

    assert_eq!(texts, vec!["I1", "I3a", "I2"])
}

#[test]
fn can_replace_all_items_of_a_list() {
    let saver = FakeSaver::new();
    let mut app = new_app_counting_saves(saver.clone());
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    post(&mut app, USRA, "/v1/list/auser", r#"{"title": "mylist"}"#);

    // We have some items
    let items_url = "/v1/list/auser/id1/items";
    post(
        &mut app,
        USRA,
        items_url,
        r#"{"ticked": true, "text": "I1"}"#,
    );
    post(
        &mut app,
        USRA,
        items_url,
        r#"{"ticked": true, "text": "I2"}"#,
    );
    post(
        &mut app,
        USRA,
        items_url,
        r#"{"ticked": true, "text": "I3"}"#,
    );

    assert_eq!(saver.num_saves(), 5);
    // Replace them all
    let (status, resp) = put(
        &mut app,
        USRA,
        items_url,
        &serde_json::json!(
            [ {"ticked": false, "text": "K1"}
            , {"ticked": true, "text": "K2", "order": 19.0}
            , { "ticked": false
              , "text": "K3"
              , "order": 30.0
              , "itemid": "0001"
              }
            , {"ticked": false, "text": "K4", "order": 35.0}
            , {"ticked": false, "text": "K5"}
            ]
        )
        .to_string(),
    );

    assert_eq!(status, 200);
    assert_eq!(saver.num_saves(), 6);
    let resp = parse(&resp);
    assert_eq!(resp[0]["text"].as_str().unwrap(), "K1");
    assert_eq!(resp[1]["text"].as_str().unwrap(), "K2");
    assert_eq!(resp[2]["text"].as_str().unwrap(), "K3");
    assert_eq!(resp[3]["text"].as_str().unwrap(), "K4");
    assert_eq!(resp[4]["text"].as_str().unwrap(), "K5");

    // They were replaced
    let all_items = parse(&get(&mut app, USRA, items_url).1);

    let tickeds: Vec<bool> = all_items
        .as_array()
        .unwrap()
        .iter()
        .map(|item| item["ticked"].as_bool().unwrap())
        .collect();

    let texts: Vec<&str> = all_items
        .as_array()
        .unwrap()
        .iter()
        .map(|item| item["text"].as_str().unwrap())
        .collect();

    let ids: Vec<&str> = all_items
        .as_array()
        .unwrap()
        .iter()
        .map(|item| item["itemid"].as_str().unwrap())
        .collect();

    let orders: Vec<f64> = all_items
        .as_array()
        .unwrap()
        .iter()
        .map(|item| item["order"].as_f64().unwrap())
        .collect();

    assert_eq!(texts, vec!["K1", "K2", "K3", "K4", "K5"]);
    assert_eq!(tickeds, vec![false, true, false, false, false]);
    assert_eq!(ids, vec!["0002", "0003", "0001", "0004", "0005"]);
    assert_eq!(
        orders,
        vec![
            16.0 as f64,
            19.0 as f64,
            30.0 as f64,
            35.0 as f64,
            51.0 as f64
        ]
    );
}

#[test]
fn can_tick_or_modify_an_item() {
    let saver = FakeSaver::new();
    let mut app = new_app_counting_saves(saver.clone());
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    post(&mut app, USRA, "/v1/list/auser", r#"{"title": "mylist"}"#);
    put(
        &mut app,
        USRA,
        "/v1/list/auser/id1/items",
        &serde_json::json!(
            [ {"itemid": "0001", "ticked": false, "text": "I1"}
            , {"itemid": "0002", "ticked": true, "text": "I2"}
            ]
        )
        .to_string(),
    );
    assert_eq!(saver.num_saves(), 3);

    test_req(
        get(&mut app, USRA, "/v1/list/auser/id1/items/0001/ticked"),
        r#"200 false"#,
    );

    test_req(
        get(&mut app, USRA, "/v1/list/auser/id1/items/0002/ticked"),
        r#"200 true"#,
    );

    test_req(
        get(&mut app, USRA, "/v1/list/auser/id1/items/0001/text"),
        r#"200 "I1""#,
    );

    test_req(
        get(&mut app, USRA, "/v1/list/auser/id1/items/0002/text"),
        r#"200 "I2""#,
    );

    test_req(
        put(
            &mut app,
            USRA,
            "/v1/list/auser/id1/items/0001/ticked",
            "true",
        ),
        r#"204 "#,
    );
    assert_eq!(saver.num_saves(), 4);

    test_req(
        put(
            &mut app,
            USRA,
            "/v1/list/auser/id1/items/0002/text",
            r#""foo""#,
        ),
        r#"204 "#,
    );
    assert_eq!(saver.num_saves(), 5);

    assert_eq!(
        parse(&get(&mut app, USRA, "/v1/list/auser/id1/items").1),
        serde_json::json!(
            [ { "itemid": "0001"
              , "order": 16.0
              , "ticked": true
              , "text": "I1"
              }
            , { "itemid": "0002"
              , "order": 32.0
              , "ticked": true
              , "text": "foo"
              }
            ]
        )
    );
}

#[test]
fn modifying_an_items_order_with_put_reorders_items() {
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    let mylist = r#"{"title": "mylist"}"#;
    post(&mut app, USRA, "/v1/list/auser", mylist);
    put(
        &mut app,
        USRA,
        "/v1/list/auser/id1/items",
        &serde_json::json!(
            [ {"itemid": "0001", "ticked": false, "text": "I1"}
            , {"itemid": "0002", "ticked": true, "text": "I2"}
            , {"itemid": "0003", "ticked": true, "text": "I3"}
            ]
        )
        .to_string(),
    );

    let item1_order: f64 =
        get_f64(&mut app, USRA, "/v1/list/auser/id1/items/0001/order");
    let item2_order: f64 =
        get_f64(&mut app, USRA, "/v1/list/auser/id1/items/0002/order");

    let new_item3_order = (item1_order + item2_order) / 2.0;
    test_req(
        put(
            &mut app,
            USRA,
            "/v1/list/auser/id1/items/0003/order",
            &serde_json::json!(new_item3_order).to_string(),
        ),
        r#"204 "#,
    );

    assert_eq!(
        parse(&get(&mut app, USRA, "/v1/list/auser/id1/items").1),
        serde_json::json!(
            [ { "itemid": "0001"
              , "order": 16.0
              , "ticked": false
              , "text": "I1"
              }
            , { "itemid": "0003"
              , "order": 24.0
              , "ticked": true
              , "text": "I3"
              }
            , { "itemid": "0002"
              , "order": 32.0
              , "ticked": true
              , "text": "I2"
              }
            ]
        )
    );
}

#[test]
fn putting_wrong_column_type_is_an_error() {
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    post(&mut app, USRA, "/v1/list/auser", r#"{"title": "mylist"}"#);
    post(
        &mut app,
        USRA,
        "/v1/list/auser/id1/items",
        r#"{"ticked": false, "text": "I1"}"#,
    );

    test_req(
        put(
            &mut app,
            USRA,
            "/v1/list/auser/id1/items/0001/ticked",
            "333",
        ),
        "400 \
         {\"error\":\"Wrong type: the column 'ticked' needs \
         values of type 'bool'.\"}",
    );

    test_req(
        put(&mut app, USRA, "/v1/list/auser/id1/items/0001/text", "true"),
        "400 \
         {\"error\":\"Wrong type: the column 'text' needs \
         values of type 'string'.\"}",
    );

    // It was not modified
    test_req(
        get(&mut app, USRA, "/v1/list/auser/id1/items/0001/text"),
        r#"200 "I1""#,
    );
}

#[test]
fn users_cannot_see_or_modify_each_others_lists() {
    let alice = LogInAs {
        username: "alice",
        password: "a",
    };
    let bob = LogInAs {
        username: "bob",
        password: "b",
    };

    // Given some users exist
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());

    post(&mut app, USRA, "/v1/user", &alice.creation_json());
    post(&mut app, USRA, "/v1/user", &bob.creation_json());

    // and alice has some lists
    post(&mut app, alice, "/v1/list/alice", r#"{"title": "L1"}"#);
    post(&mut app, alice, "/v1/list/alice", r#"{"title": "L2"}"#);
    post(
        &mut app,
        alice,
        "/v1/list/alice/id1/items",
        r#"{"ticked": false, "text": "I1"}"#,
    );

    // Then bob cannot list them
    test_req(
        get(&mut app, bob, "/v1/list/alice"),
        r#"403 {"error":"Permission denied."}"#,
    );
    // Or get them
    test_req(
        get(&mut app, bob, "/v1/list/alice/id1"),
        r#"403 {"error":"Permission denied."}"#,
    );
    // Or add one
    test_req(
        post(&mut app, bob, "/v1/list/alice", r#"{"title": "id1"}"#),
        r#"403 {"error":"Permission denied."}"#,
    );
    // Or update one
    test_req(
        put(&mut app, bob, "/v1/list/alice/id1/items", "[]"),
        r#"403 {"error":"Permission denied."}"#,
    );
    test_req(
        post(
            &mut app,
            bob,
            "/v1/list/alice/id1/items",
            r#"{"ticked": false, "text": "I2"}"#,
        ),
        r#"403 {"error":"Permission denied."}"#,
    );
    test_req(
        put(
            &mut app,
            bob,
            "/v1/list/alice/id1/items/0001",
            r#"{"ticked": false, "text": "I1updated", "order": 16.0}"#,
        ),
        r#"403 {"error":"Permission denied."}"#,
    );
    test_req(
        put(
            &mut app,
            bob,
            "/v1/list/alice/id1/items/0001/ticked",
            "true",
        ),
        r#"403 {"error":"Permission denied."}"#,
    );
}

#[test]
fn users_lists_disappear_when_they_are_deleted() {
    let alice = LogInAs {
        username: "alice",
        password: "a",
    };

    // Given a user
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    post(&mut app, USRA, "/v1/user", &alice.creation_json());

    // with some lists
    post(&mut app, alice, "/v1/list/alice", r#"{"title": "L1"}"#);

    // When I delete the user
    delete(&mut app, USRA, "/v1/user/alice");

    // The lists are gone (actually we can't see them because
    // only alice can)
    test_req(
        get(&mut app, alice, "/v1/list/alice"),
        r#"401 Invalid username or password"#,
    );
}

#[test]
fn lists_can_be_deleted() {
    // Given a user with lists
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    post(&mut app, USRA, "/v1/list/auser", r#"{"title": "1"}"#);
    post(&mut app, USRA, "/v1/list/auser", r#"{"title": "2"}"#);
    post(&mut app, USRA, "/v1/list/auser", r#"{"title": "3"}"#);

    // When I delete a list
    test_req(delete(&mut app, USRA, "/v1/list/auser/id2"), r#"204 "#);

    // It is gone
    test_req(
        get(&mut app, USRA, "/v1/list/auser"),
        &format!(
            "200 [{},{}]",
            r#"{"listid":"id1","title":"1"}"#,
            r#"{"listid":"id3","title":"3"}"#,
        ),
    );
}

#[test]
fn items_can_be_deleted() {
    // Given a list with items
    let mut app = new_app();
    post_nou(&mut app, "/v1/setup", &USRA.creation_json());
    post(&mut app, USRA, "/v1/list/auser", r#"{"title": "1"}"#);
    put(
        &mut app,
        USRA,
        "/v1/list/auser/id1/items",
        &serde_json::json!(
            [ { "itemid": "0001"
              , "order": 16.0
              , "text": "I1"
              , "ticked": false
              }
            , { "itemid": "0002"
              , "order": 24.0
              , "text": "I2"
              , "ticked": true
              }
            , { "itemid": "0003"
              , "order": 32.0
              , "text": "I3"
              , "ticked": true
              }
            ]
        )
        .to_string(),
    );

    // When I delete an item
    test_req(
        delete(&mut app, USRA, "/v1/list/auser/id1/items/0002"),
        r#"204 "#,
    );

    // It is gone
    assert_eq!(
        parse(&get(&mut app, USRA, "/v1/list/auser/id1/items").1),
        serde_json::json!(
            [ { "itemid": "0001"
              , "order": 16.0
              , "text": "I1"
              , "ticked": false
              }
            , { "itemid": "0003"
              , "order": 32.0
              , "text": "I3"
              , "ticked": true
              }
            ]
        ),
    );
}

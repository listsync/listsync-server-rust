use actix_http::body::Body;
use actix_http::error::Error;
use actix_http::Request;
use actix_web::dev::{Service, ServiceResponse};
use actix_web::http::StatusCode;
use actix_web::test;
use actix_web::web::Bytes;

use crate::utils::log_in_as::LogInAs;
use crate::utils::requests::req;

pub fn delete_with_payload<'a, S>(
    app: &'a mut S,
    log_in_as: LogInAs,
    path: &str,
    payload: &str,
) -> (StatusCode, Bytes)
where
    S: Service<
        Request = Request,
        Response = ServiceResponse<Body>,
        Error = Error,
    >,
{
    req(
        app,
        Some(log_in_as),
        test::TestRequest::delete()
            .header("Content-Type", "application/json")
            .set_payload(Bytes::from(String::from(payload))),
        path,
    )
}

pub fn put_nou<'a, S>(
    app: &'a mut S,
    path: &str,
    payload: &str,
) -> (StatusCode, Bytes)
where
    S: Service<
        Request = Request,
        Response = ServiceResponse<Body>,
        Error = Error,
    >,
{
    req(
        app,
        None,
        test::TestRequest::put()
            .header("Content-Type", "application/json")
            .set_payload(Bytes::from(String::from(payload))),
        path,
    )
}

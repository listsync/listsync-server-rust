#![allow(dead_code)]

use actix_http::error::Error;
use actix_http::Request;
use actix_web::dev::{Service, ServiceResponse};
use actix_web::{test, web, App};
use futures::executor;
use std::cell::Cell;
use std::sync::{Arc, Mutex};
use std::time::{Duration, SystemTime};

use listsync_server_rust;
use listsync_server_rust::clock::Clock;
use listsync_server_rust::db::memory_database::{MemoryDatabase, Users};
use listsync_server_rust::idgen::IdGen;
use listsync_server_rust::privacy_page::PrivacyPage;
use listsync_server_rust::save::Saver;
use listsync_server_rust::state::State;
use listsync_server_rust::token_generator::TokenGenerator;

use crate::utils::fake_password_handler::FakePasswordHandler;

const FAKE_PASSWORD_HANDLER: FakePasswordHandler = FakePasswordHandler {};

pub struct FakeIdGen {
    i: Mutex<Cell<u32>>,
}

impl FakeIdGen {
    pub fn new() -> FakeIdGen {
        FakeIdGen {
            i: Mutex::new(Cell::new(0)),
        }
    }
}

impl IdGen for FakeIdGen {
    fn gen(&self) -> String {
        let mut i = self.i.lock().unwrap();
        *i.get_mut() += 1;
        format!("id{}", i.get())
    }
}

#[derive(Clone)]
pub struct FakeSaver {
    pub save_counter: Arc<Mutex<Cell<u32>>>,
}

impl FakeSaver {
    pub fn new() -> FakeSaver {
        FakeSaver {
            save_counter: Arc::new(Mutex::new(Cell::new(0))),
        }
    }

    pub fn num_saves(&self) -> u32 {
        self.save_counter.lock().unwrap().get()
    }
}

impl Saver<Users> for FakeSaver {
    fn save(&self, _users: &Users) {
        let c = self.save_counter.lock().unwrap();
        c.set(c.get() + 1);
    }

    fn load(&self) -> Users {
        Users::new()
    }
}

pub struct FakeTokenGenerator {
    hardcoded_token: String,
}

impl FakeTokenGenerator {
    pub fn new(hardcoded_token: &str) -> FakeTokenGenerator {
        FakeTokenGenerator {
            hardcoded_token: String::from(hardcoded_token),
        }
    }
}

impl TokenGenerator for FakeTokenGenerator {
    fn generate_login_token(&self) -> String {
        self.hardcoded_token.clone()
    }

    fn generate_registration_code(&self) -> String {
        self.hardcoded_token.clone()
    }
}

#[derive(Clone)]
pub struct FakeClock {
    hardcoded_time: Arc<Mutex<Cell<SystemTime>>>,
}

impl FakeClock {
    pub fn new(hardcoded_time: SystemTime) -> FakeClock {
        FakeClock {
            hardcoded_time: Arc::new(Mutex::new(Cell::new(hardcoded_time))),
        }
    }

    pub fn advance_time(&mut self, duration: Duration) {
        *self.hardcoded_time.lock().unwrap().get_mut() += duration;
    }
}

impl Clock for FakeClock {
    fn now(&self) -> SystemTime {
        self.hardcoded_time.lock().unwrap().get().clone()
    }
}

pub fn new_app(
) -> impl Service<Request = Request, Response = ServiceResponse, Error = Error>
{
    new_app_all(
        FakeSaver::new(),
        FakeIdGen::new(),
        FakeTokenGenerator::new(""),
        FakeClock::new(SystemTime::UNIX_EPOCH),
    )
}

pub fn new_app_with_tokens(
    token_generator: FakeTokenGenerator,
) -> impl Service<Request = Request, Response = ServiceResponse, Error = Error>
{
    new_app_all(
        FakeSaver::new(),
        FakeIdGen::new(),
        token_generator,
        FakeClock::new(SystemTime::UNIX_EPOCH),
    )
}

pub fn new_app_with_tokens_and_clock(
    token_generator: FakeTokenGenerator,
    clock: FakeClock,
) -> impl Service<Request = Request, Response = ServiceResponse, Error = Error>
{
    new_app_all(FakeSaver::new(), FakeIdGen::new(), token_generator, clock)
}

pub fn new_app_counting_saves(
    saver: FakeSaver,
) -> impl Service<Request = Request, Response = ServiceResponse, Error = Error>
{
    new_app_all(
        saver,
        FakeIdGen::new(),
        FakeTokenGenerator::new(""),
        FakeClock::new(SystemTime::UNIX_EPOCH),
    )
}

pub fn new_app_all(
    saver: FakeSaver,
    idgen: FakeIdGen,
    token_generator: FakeTokenGenerator,
    clock: FakeClock,
) -> impl Service<Request = Request, Response = ServiceResponse, Error = Error>
{
    executor::block_on(test::init_service(
        App::new()
            .app_data(web::Data::new(State::new(
                Box::new(MemoryDatabase::new(
                    Box::new(saver),
                    &FAKE_PASSWORD_HANDLER,
                    Box::new(idgen),
                )),
                Box::new(token_generator),
                Box::new(clock),
                PrivacyPage::new(String::from(
                    "<h2>PRIVACY_INFO</h2><p>foo</p>",
                )),
            )))
            .configure(listsync_server_rust::config_unauthenticated)
            .configure(listsync_server_rust::config),
    ))
}

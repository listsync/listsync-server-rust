use actix_web_httpauth::headers::authorization::Basic;

#[derive(Clone, Copy)]
pub struct LogInAs {
    pub username: &'static str,
    pub password: &'static str,
}

impl LogInAs {
    pub fn basic(&self) -> Basic {
        Basic::new(self.username, Some(self.password))
    }

    pub fn creation_json(&self) -> String {
        format!(
            r#"{{"username": "{}", "password":"{}"}}"#,
            self.username, self.password
        )
    }
}

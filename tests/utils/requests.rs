use actix_http::body::Body;
use actix_http::error::Error;
use actix_http::{Request, Response};
use actix_web::dev::{Service, ServiceResponse};
use actix_web::http::StatusCode;
use actix_web::test;
use actix_web::web::{Bytes, BytesMut, HttpResponse};
use futures::executor;
use futures::stream::StreamExt;

use crate::utils::log_in_as::LogInAs;

async fn read_response_body(mut res: Response<Body>) -> Bytes {
    let mut body = res.take_body();
    let mut bytes = BytesMut::new();
    while let Some(item) = body.next().await {
        bytes.extend_from_slice(&item.unwrap());
    }
    bytes.freeze()
}

pub fn req<'a, S>(
    app: &'a mut S,
    log_in_as: Option<LogInAs>,
    req_type: test::TestRequest,
    path: &str,
) -> (StatusCode, Bytes)
where
    S: Service<
        Request = Request,
        Response = ServiceResponse<Body>,
        Error = Error,
    >,
{
    let req = req_type.uri(path);
    let req = if let Some(lia) = log_in_as {
        req.header("Authorization", lia.basic())
    } else {
        req
    };

    let resp = executor::block_on(app.call(req.to_request()))
        .map(|sr| sr.into())
        .unwrap_or_else(|e| HttpResponse::from_error(e));

    (resp.status(), executor::block_on(read_response_body(resp)))
}

pub fn get<'a, S>(
    app: &'a mut S,
    log_in_as: LogInAs,
    path: &str,
) -> (StatusCode, Bytes)
where
    S: Service<
        Request = Request,
        Response = ServiceResponse<Body>,
        Error = Error,
    >,
{
    req(app, Some(log_in_as), test::TestRequest::get(), path)
}

pub fn get_nou<'a, S>(app: &'a mut S, path: &str) -> (StatusCode, Bytes)
where
    S: Service<
        Request = Request,
        Response = ServiceResponse<Body>,
        Error = Error,
    >,
{
    req(app, None, test::TestRequest::get(), path)
}

pub fn put<'a, S>(
    app: &'a mut S,
    log_in_as: LogInAs,
    path: &str,
    payload: &str,
) -> (StatusCode, Bytes)
where
    S: Service<
        Request = Request,
        Response = ServiceResponse<Body>,
        Error = Error,
    >,
{
    req(
        app,
        Some(log_in_as),
        test::TestRequest::put()
            .header("Content-Type", "application/json")
            .set_payload(Bytes::from(String::from(payload))),
        path,
    )
}

pub fn post<'a, S>(
    app: &'a mut S,
    log_in_as: LogInAs,
    path: &str,
    payload: &str,
) -> (StatusCode, Bytes)
where
    S: Service<
        Request = Request,
        Response = ServiceResponse<Body>,
        Error = Error,
    >,
{
    req(
        app,
        Some(log_in_as),
        test::TestRequest::post()
            .header("Content-Type", "application/json")
            .set_payload(Bytes::from(String::from(payload))),
        path,
    )
}

pub fn post_nou<'a, S>(
    app: &'a mut S,
    path: &str,
    payload: &str,
) -> (StatusCode, Bytes)
where
    S: Service<
        Request = Request,
        Response = ServiceResponse<Body>,
        Error = Error,
    >,
{
    req(
        app,
        None,
        test::TestRequest::post()
            .header("Content-Type", "application/json")
            .set_payload(Bytes::from(String::from(payload))),
        path,
    )
}

pub fn delete<'a, S>(
    app: &'a mut S,
    log_in_as: LogInAs,
    path: &str,
) -> (StatusCode, Bytes)
where
    S: Service<
        Request = Request,
        Response = ServiceResponse<Body>,
        Error = Error,
    >,
{
    req(app, Some(log_in_as), test::TestRequest::delete(), path)
}

pub fn test_req((status, body): (StatusCode, Bytes), exp: &str) {
    assert_eq!(
        format!(
            "{} {}",
            status.as_u16(),
            String::from_utf8(body[..].to_vec()).unwrap()
        ),
        exp
    )
}

use listsync_server_rust::password_handler::PasswordHandler;

pub struct FakePasswordHandler {}

impl PasswordHandler for FakePasswordHandler {
    fn hash_password(&self, password: &str) -> String {
        String::from(password)
    }

    fn is_correct_password(
        &self,
        possible_password: &str,
        user_hashed_password: &str,
    ) -> bool {
        possible_password == user_hashed_password
    }
}

## Notes to self about deployment.

Build and upload:

```bash
make deploy
```

There is a screen running in my webspace.  To attach:

```bash
screen -r
```

Make sure you are in the right screen for this program (screen 0), then press
Ctrl-c and:

```
mv listsync-server-rust.new listsync-server-rust
./listsync-server-rust
```

I have set up my web provider to proxy
https://listsync.artificialworlds.net/api to the process running in the screen,
on port 8088.

## Backup

The backup is run daily at 0123 UTC via cron on the filesandy user of my
server.  It copies the state database into a daily and monthly backup file.
